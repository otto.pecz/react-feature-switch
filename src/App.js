import React from 'react';
import './App.css';

const FeatureContext = React.createContext(['foo'])

function ActiveComponent () {
  return (<code>feature active</code>)
}

function InactiveComponent () {
  return (<code>feature inactive</code>)
}

class FeatureRouter extends React.Component {
  static contextType = FeatureContext

  fallback () {
    return (<code>fallback</code>)
  }

  render () {
    return (this.context.includes(this.props.featureName))
      ? this.props.activeComponent
      : this.props.inactiveComponent || (<this.fallback/>)
  }
}

class App extends React.Component {
  render () {
    return (
      <div className="App">
        <FeatureRouter
          featureName = 'foo'
          activeComponent = {<ActiveComponent />}
          inactiveComponent = {<InactiveComponent />}
        />
      </div>
    );
  }
}

export default App;
